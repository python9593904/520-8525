import os
import sys

def limpar_tela():
    os.system('cls' if os.name == 'nt' else 'clear')

def mostrar_menu_principal():
    print("\nQuitanda:")
    print("1: Ver cesta")
    print("2: Adicionar frutas")
    print("3: Sair")

def mostrar_menu_frutas():
    print("\nEscolha a fruta desejada:")
    print("1 - Banana")
    print("2 - Melancia")
    print("3 - Morango")

def adicionar_fruta(cesta, escolha):
    frutas = {1: "Banana", 2: "Melancia", 3: "Morango"}
    if escolha in frutas:
        cesta.append(frutas[escolha])
        print(f"{frutas[escolha]} adicionada com sucesso!")
    else:
        print("Digite uma opção válida!")

def obter_opcao_valida(prompt, opcoes_validas):
    while True:
        try:
            escolha = int(input(prompt))
            if escolha in opcoes_validas:
                return escolha
            else:
                print("Digite uma opção válida!")
        except ValueError:
            print("Por favor, digite um número válido.")

def main():
    cesta = []
    while True:
        mostrar_menu_principal()
        opcao = obter_opcao_valida("Digite a opção desejada: ", [1, 2, 3])

        if opcao == 1:
            print("Cesta de compras:", cesta)
        elif opcao == 2:
            mostrar_menu_frutas()
            escolha_fruta = obter_opcao_valida("Digite a opção desejada: ", [1, 2, 3])
            adicionar_fruta(cesta, escolha_fruta)
        elif opcao == 3:
            print("Obrigado por visitar a quitanda! Até logo!")
            sys.exit()

        input("\nPressione Enter para continuar...")
        limpar_tela()

if __name__ == "__main__":
    main()
