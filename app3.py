num1 = int(input("Digite o primeiro número inteiro: "))
num2 = int(input("Digite o segundo número inteiro: "))

soma = num1 + num2
diferenca = num1 - num2

print("-" * 30)
print(f"Soma: {num1} + {num2} = {soma}")
print(f"Diferença: {num1} - {num2} = {diferenca}")
