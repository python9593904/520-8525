def input_numero_float(mensagem):
    while True:
        entrada = input(mensagem)
        try:
            return float(entrada)
        except ValueError:
            print("Por favor, digite um número válido.")

def formatar_numero(num):
    return int(num) if num.is_integer() else num

num1 = input_numero_float("Digite o primeiro número: ")
num2 = input_numero_float("Digite o segundo número: ")
soma = num1 + num2
diferenca = num1 - num2

num1_formatado = formatar_numero(num1)
num2_formatado = formatar_numero(num2)
soma_formatada = formatar_numero(soma)
diferenca_formatada = formatar_numero(diferenca)

print("-" * 30)
print(f"Soma: {num1_formatado} + {num2_formatado} = {soma_formatada}")
print(f"Diferença: {num1_formatado} - {num2_formatado} = {diferenca_formatada}")
