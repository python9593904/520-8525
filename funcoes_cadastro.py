import csv

def validar_cpf(cpf):
    if not (cpf.isdigit() and len(cpf) == 11):
        print("CPF inválido. O CPF deve conter exatamente 11 dígitos numéricos.")
        return False
    return True

def cadastrar_usuario():
    while True:
        cpf = input("Digite o CPF (apenas números): ")
        if validar_cpf(cpf):
            break
        print("CPF inválido. Tente novamente.")

    nome = input("Digite o nome: ").strip().title()
    while True:
        try:
            idade = int(input("Digite a idade: "))
            break
        except ValueError:
            print("Idade inválida. Tente novamente.")

    while True:
        sexo = input("Digite o sexo (M/F/Outro): ").strip().capitalize()
        if sexo in ['M', 'F', 'Outro']:
            break
        print("Sexo inválido. Tente novamente.")

    endereco = input("Digite o endereço: ").strip().title()

    return [cpf, nome, idade, sexo, endereco]

def salvar_em_csv(dados_usuario, nome_arquivo='cadastro.csv'):
    try:
        with open(nome_arquivo, mode='a', newline='', encoding='utf-8') as arquivo:
            escritor_csv = csv.writer(arquivo, delimiter=';')
            escritor_csv.writerow(dados_usuario)
            print("Usuário cadastrado com sucesso.")
    except IOError:
        print("Erro ao salvar o cadastro. Por favor, verifique se o arquivo não está em uso ou se você tem permissão de escrita.")

def cpf_ja_cadastrado(cpf, nome_arquivo='cadastro.csv'):
    try:
        with open(nome_arquivo, mode='r', encoding='utf-8') as arquivo:
            leitor_csv = csv.reader(arquivo, delimiter=';')
            for linha in leitor_csv:
                if linha[0] == cpf:
                    return True
    except IOError:
        print("Erro ao acessar o arquivo. Não foi possível verificar se o CPF já está cadastrado.")
    return False

def consultar_cadastro(cpf_pesquisa, nome_arquivo='cadastro.csv'):
    try:
        with open(nome_arquivo, mode='r', encoding='utf-8') as arquivo:
            leitor_csv = csv.reader(arquivo, delimiter=';')
            for linha in leitor_csv:
                if linha[0] == cpf_pesquisa:
                    print("Cadastro encontrado:")
                    print(f"CPF: {linha[0]}, Nome: {linha[1]}, Idade: {linha[2]}, Sexo: {linha[3]}, Endereço: {linha[4]}")
                    return
            print("Cadastro não encontrado.")
    except IOError:
        print("Erro ao ler o arquivo. Não foi possível consultar o cadastro.")

def excluir_cadastro(cpf_exclusao, nome_arquivo='cadastro.csv'):
    linhas = []
    encontrou = False

    try:
        with open(nome_arquivo, mode='r', encoding='utf-8') as arquivo:
            leitor_csv = csv.reader(arquivo, delimiter=';')
            for linha in leitor_csv:
                if linha[0] != cpf_exclusao:
                    linhas.append(linha)
                else:
                    encontrou = True

        if encontrou:
            with open(nome_arquivo, mode='w', newline='', encoding='utf-8') as arquivo:
                escritor_csv = csv.writer(arquivo, delimiter=';')
                escritor_csv.writerows(linhas)
            print("Cadastro excluído com sucesso.")
        else:
            print("Cadastro não encontrado.")
    except IOError:
        print("Erro ao acessar o arquivo. Não foi possível excluir o cadastro.")

def buscar_por_nome(nome_busca, nome_arquivo='cadastro.csv'):
    encontrou = False
    try:
        with open(nome_arquivo, mode='r', encoding='utf-8') as arquivo:
            leitor_csv = csv.reader(arquivo, delimiter=';')
            for linha in leitor_csv:
                if nome_busca.lower() in linha[1].lower():
                    print(f"CPF: {linha[0]}, Nome: {linha[1]}, Idade: {linha[2]}, Sexo: {linha[3]}, Endereço: {linha[4]}")
                    encontrou = True
            if not encontrou:
                print("Nenhum cadastro encontrado com esse nome.")
    except IOError:
        print("Erro ao ler o arquivo. Não foi possível realizar a busca pelo nome.")


