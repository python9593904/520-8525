import sqlite3
import random

def conectar_bd():
    return sqlite3.connect('lanchonete.db')

def criar_tabelas(conn):
    cursor = conn.cursor()
    cursor.execute('''
    CREATE TABLE IF NOT EXISTS comidas (
        nome TEXT PRIMARY KEY,
        preco INTEGER,
        quantidade INTEGER,
        quantidade_inicial INTEGER DEFAULT 0
    )''')
    cursor.execute('''
    CREATE TABLE IF NOT EXISTS bebidas (
        nome TEXT PRIMARY KEY,
        preco INTEGER,
        quantidade INTEGER,
        quantidade_inicial INTEGER DEFAULT 0
    )''')
    cursor.execute('''
    CREATE TABLE IF NOT EXISTS historico_pedidos (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        comida TEXT,
        bebida TEXT,
        preco_total INTEGER,
        data_pedido TIMESTAMP DEFAULT CURRENT_TIMESTAMP
    )''')
    conn.commit()

def popular_tabelas(conn):
    comidas = [('Hamburguer', 10, 5), ('Pizza', 12, 5), ('Sanduíche', 8, 5), ('Salada', 7, 5), ('Batata Frita', 5, 5)]
    bebidas = [('Refrigerante', 3, 7), ('Água', 2, 7), ('Suco', 4, 7)]

    cursor = conn.cursor()
    cursor.executemany('INSERT OR IGNORE INTO comidas (nome, preco, quantidade, quantidade_inicial) VALUES (?, ?, ?, ?)', [(nome, preco, qtd, qtd) for nome, preco, qtd in comidas])
    cursor.executemany('INSERT OR IGNORE INTO bebidas (nome, preco, quantidade, quantidade_inicial) VALUES (?, ?, ?, ?)', [(nome, preco, qtd, qtd) for nome, preco, qtd in bebidas])
    conn.commit()

def selecionar_item_aleatorio(tabela, conn):
    cursor = conn.cursor()
    cursor.execute(f'SELECT nome FROM {tabela}')
    return random.choice(cursor.fetchall())[0]

def verificar_e_atualizar_estoque(comida, bebida, conn):
    cursor = conn.cursor()
    try:
        cursor.execute('BEGIN')

        cursor.execute('SELECT quantidade FROM comidas WHERE nome = ?', (comida,))
        qtd_comida = cursor.fetchone()[0]

        cursor.execute('SELECT quantidade FROM bebidas WHERE nome = ?', (bebida,))
        qtd_bebida = cursor.fetchone()[0]

        if qtd_comida > 0 and qtd_bebida > 0:
            cursor.execute('UPDATE comidas SET quantidade = quantidade - 1 WHERE nome = ?', (comida,))
            cursor.execute('UPDATE bebidas SET quantidade = quantidade - 1 WHERE nome = ?', (bebida,))
            cursor.execute('COMMIT')
            return True
        else:
            cursor.execute('ROLLBACK')
            return False
    except sqlite3.Error:
        cursor.execute('ROLLBACK')
        return False

def calcular_preco_total(comida, bebida, conn):
    cursor = conn.cursor()
    cursor.execute('SELECT preco FROM comidas WHERE nome = ?', (comida,))
    preco_comida = cursor.fetchone()[0]

    cursor.execute('SELECT preco FROM bebidas WHERE nome = ?', (bebida,))
    preco_bebida = cursor.fetchone()[0]

    return preco_comida + preco_bebida

def registrar_pedido(conn, comida, bebida, preco_total):
    cursor = conn.cursor()
    cursor.execute('''
    INSERT INTO historico_pedidos (comida, bebida, preco_total)
    VALUES (?, ?, ?)
    ''', (comida, bebida, preco_total))
    conn.commit()

def gerar_relatorio(conn):
    cursor = conn.cursor()
    cursor.execute('SELECT * FROM historico_pedidos')
    return cursor.fetchall()

def repor_estoque(conn):
    cursor = conn.cursor()
    tabelas = ['comidas', 'bebidas']
    for tabela in tabelas:
        cursor.execute(f'SELECT nome, quantidade_inicial FROM {tabela}')
        itens = cursor.fetchall()
        for nome, qtd_inicial in itens:
            cursor.execute(f'UPDATE {tabela} SET quantidade = quantidade_inicial / 2 WHERE nome = ?', (nome,))
    conn.commit()

def fazer_pedido(conn):
    comida = selecionar_item_aleatorio('comidas', conn)
    bebida = selecionar_item_aleatorio('bebidas', conn)

    if verificar_e_atualizar_estoque(comida, bebida, conn):
        preco_total = calcular_preco_total(comida, bebida, conn)
        registrar_pedido(conn, comida, bebida, preco_total)
        repor_estoque(conn) 
        return f"Pedido: {comida} e {bebida}. Total a pagar: R${preco_total}."
    else:
        return "Desculpe, um ou mais itens do seu pedido não estão disponíveis."

def main():
    conn = conectar_bd()
    criar_tabelas(conn)
    popular_tabelas(conn)

    for _ in range(5):
        print(fazer_pedido(conn))

    print("Relatório de Pedidos:")
    for pedido in gerar_relatorio(conn):
        print(pedido)

    conn.close()

if __name__ == "__main__":
    main()
