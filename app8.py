def contar_pares(numeros):
    eh_par = lambda x: x % 2 == 0
    numeros_pares = list(filter(eh_par, numeros))
    return len(numeros_pares)

numeros_usuario = input("Digite uma lista de números separados por espaço: ")
numeros_lista = [int(n) for n in numeros_usuario.split()]

quantidade_pares = contar_pares(numeros_lista)
print(f"Número de números pares: {quantidade_pares}")
