soma = lambda x, y: x + y
subtracao = lambda x, y: x - y
multiplicacao = lambda x, y: x * y
divisao = lambda x, y: x / y if y != 0 else 'Erro: Divisão por zero'

while True:
    num1 = float(input("Digite o primeiro número: "))
    num2 = float(input("Digite o segundo número: "))

    print("\nEscolha a operação desejada:")
    print("1. Soma")
    print("2. Subtração")
    print("3. Multiplicação")
    print("4. Divisão")

    escolha = input("Escolha uma operação (1/2/3/4): ")

    if escolha == '1':
        resultado = soma(num1, num2)
    elif escolha == '2':
        resultado = subtracao(num1, num2)
    elif escolha == '3':
        resultado = multiplicacao(num1, num2)
    elif escolha == '4':
        resultado = divisao(num1, num2)
    else:
        print("Opção inválida! Tente novamente.")
        continue

    print("Resultado:", resultado)

    continuar = input("Deseja realizar outra operação? (sim/não): ").lower()
    if continuar != 'sim':
        print("Obrigado por usar a calculadora. Até mais!")
        break
