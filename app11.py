import random
import time

def realizar_sorteio(total_participantes, total_sorteio, lista):
    participantes = random.sample(lista, total_participantes)
    vencedores = random.sample(participantes, total_sorteio)
    return vencedores

def exibir_vencedor_com_suspense(vencedor):
    for letra in vencedor:
        print(letra, end='', flush=True)
        time.sleep(0.5)  
    print()  

def main():
    lista = ["Joao", "Maria", "Tiago", "Amanda", "Emanuele", "Caio", "Suzana", "Miguel", 
             "Rosangela", "Rian", "Lucimar", "Ulisses", "Leonardo", "Kaique", "Bruno", 
             "Raquel", "Benedito", "Tereza", "Valmir", "Joaquim"]

    while True:
        try:
            total_participantes = int(input("Digite o número de pessoas para participar do sorteio (2-20): "))
            if 2 <= total_participantes <= 20:
                break
            else:
                print("Por favor, digite um número entre 2 e 20.")
        except ValueError:
            print("Por favor, digite um número válido.")

    while True:
        try:
            total_sorteio = int(input("Digite o número de pessoas a serem sorteadas: "))
            if 1 <= total_sorteio <= total_participantes:
                break
            else:
                print(f"Por favor, digite um número entre 1 e {total_participantes}.")
        except ValueError:
            print("Por favor, digite um número válido.")

    vencedores = realizar_sorteio(total_participantes, total_sorteio, lista)
    print("\nPreparando para revelar os vencedores...")
    time.sleep(2)  

    print("\nVencedores do sorteio:")
    for vencedor in vencedores:
        exibir_vencedor_com_suspense(vencedor)
        time.sleep(1) 

    print("\nParabéns aos vencedores:")
    for vencedor in vencedores:
        print(vencedor)
    print("Vocês foram os sortudos de hoje!")

if __name__ == "__main__":
    main()
