lista_produtos = []

catalogo_frutas = {
    '1': 'Banana',
    '2': 'Melancia',
    '3': 'Morango'
}

tabela_precos = {
    'Banana': 3.50,
    'Melancia': 7.50,
    'Morango': 5.00
}

while True:
    print('Quitanda')
    print('1: Ver lista de produtos')
    print('2: Adicionar Frutas')
    print('3: Finalizar compra')
    print('4: Sair')

    escolha = input('Digite a opção desejada: ')

    if escolha == '1':
        if not lista_produtos:
            print('A lista de produtos está vazia.')
        else:
            print('Lista de Produtos:')
            for produto in lista_produtos:
                print(produto)
        print('------------------------')

    elif escolha == '2':
        print('Escolha a fruta desejada:')
        for codigo, fruta in catalogo_frutas.items():
            print(f'{codigo} - {fruta}')
        escolha_fruta = input('Digite a opção desejada: ')

        if escolha_fruta in catalogo_frutas:
            lista_produtos.append(catalogo_frutas[escolha_fruta])
            print(f'{catalogo_frutas[escolha_fruta]} foi adicionado com sucesso!')
        else:
            print('Opção inválida')
        print('------------------------')

    elif escolha == '3':
        if lista_produtos:
            valor_total = sum(tabela_precos[produto] for produto in lista_produtos)
            print(f'Total da compra: R${valor_total:.2f}')
            print('Lista de produtos:', ', '.join(lista_produtos))
        else:
            print('Lista de produtos vazia.')
        print('------------------------')

    elif escolha == '4':
        print('Saindo da Quitanda...')
        break

    else:
        print('Opção Inválida')
        print('------------------------')
