def determinar_geracao(ano_nascimento):
    if ano_nascimento <= 1964:
        return "Geração Baby Boomer"
    elif 1965 <= ano_nascimento <= 1979:
        return "Geração X"
    elif 1980 <= ano_nascimento <= 1994:
        return "Geração Y"
    else:
        return "Geração Z"

def input_ano_nascimento():
    while True:
        ano = input("Digite o seu ano de nascimento: ")
        if ano.isdigit():
            return int(ano)
        else:
            print("Por favor, digite apenas números.")

ano_nascimento = input_ano_nascimento()
geracao = determinar_geracao(ano_nascimento)

print(f"Ano de nascimento = {ano_nascimento}: {geracao}")