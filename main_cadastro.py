import funcoes_cadastro

def main():
    while True:
        print("\nMenu de Opções:")
        print("1 - Cadastrar Novo Usuário")
        print("2 - Consultar Cadastro por CPF")
        print("3 - Buscar Cadastro por Nome")
        print("4 - Excluir Cadastro")
        print("0 - Sair")

        try:
            opcao = int(input("Escolha uma opção: "))
            if opcao < 0 or opcao > 4:
                raise ValueError
        except ValueError:
            print("Opção inválida. Por favor, insira um número entre 0 e 4.")
            continue

        if opcao == 1:
            dados_usuario = funcoes_cadastro.cadastrar_usuario()
            if funcoes_cadastro.cpf_ja_cadastrado(dados_usuario[0]):
                print("CPF já cadastrado.")
                continue
            funcoes_cadastro.salvar_em_csv(dados_usuario)
        elif opcao == 2:
            cpf_pesquisa = input("Digite o CPF para busca: ")
            funcoes_cadastro.consultar_cadastro(cpf_pesquisa)
        elif opcao == 3:
            nome_busca = input("Digite o nome para busca: ")
            funcoes_cadastro.buscar_por_nome(nome_busca)
        elif opcao == 4:
            cpf_exclusao = input("Digite o CPF para exclusão: ")
            funcoes_cadastro.excluir_cadastro(cpf_exclusao)
        elif opcao == 0:
            print("Saindo do programa...")
            break
        else:
            print("Opção inválida. Por favor, tente novamente.")

if __name__ == "__main__":
    main()
