def calcular_tinta(largura, altura):
    area = largura * altura
    latas_necessarias = area / 3
    return latas_necessarias

def converter_para_float(valor):
    valor_formatado = valor.replace(',', '.')
    return float(valor_formatado)

largura_str = input("Digite a largura da parede em metros (use ponto ou vírgula para decimais): ")
altura_str = input("Digite a altura da parede em metros (use ponto ou vírgula para decimais): ")

largura = converter_para_float(largura_str)
altura = converter_para_float(altura_str)

latas = calcular_tinta(largura, altura)
print(f"Você precisará de {latas:.2f} latas de tinta para pintar a parede.")
