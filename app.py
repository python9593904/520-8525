def formatar_cpf(cpf):
    return f"{cpf[:3]}.{cpf[3:6]}.{cpf[6:9]}-{cpf[9:]}"

def input_numerico(mensagem):
    while True:
        entrada = input(mensagem)
        if entrada.isdigit():
            return entrada
        else:
            print("Por favor, digite apenas números.")

nome = input("Digite seu nome: ")
cpf_numeros = input_numerico("Digite seu CPF apenas com números: ")
idade = input_numerico("Digite sua idade: ")
pais = input("Digite a sigla do seu país (ex: BR): ")

cpf_formatado = formatar_cpf(cpf_numeros)

linha_separadora = "-" * 30  
print(linha_separadora)
print("Confirmação de cadastro:")
print(f"Nome: {nome}")
print(f"CPF: {cpf_formatado}")
print(f"Idade: {idade}")
print(f"País: {pais}")
print(linha_separadora)
