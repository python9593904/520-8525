# funcoes.py

def adicionar_pessoa(dicionario):
    nome = input("Digite o nome da pessoa: ")
    idade = int(input("Digite a idade da pessoa: "))
    dicionario[nome] = idade

def classificar_idades(dicionario):
    categorias = {"Menor de idade": [], "Adulto": [], "Idoso": []}
    for nome, idade in dicionario.items():
        if idade < 18:
            categorias["Menor de idade"].append(nome)
        elif idade < 60:
            categorias["Adulto"].append(nome)
        else:
            categorias["Idoso"].append(nome)
    return categorias

def exibir_categorias(categorias):
    for categoria, nomes in categorias.items():
        print(f"{categoria}: {', '.join(nomes)}")
