from banco import FilaBanco

def menu():
    print("\nBem-vindo ao Sistema de Fila do Banco")
    print("1 - Adicionar pessoa à fila")
    print("2 - Atender pessoa da fila")
    print("3 - Dar prioridade a idoso")
    print("4 - Sair")
    return input("Escolha uma opção: ")

def main():
    fila = FilaBanco()
    while True:
        opcao = menu()
        if opcao == "1":
            idade = input("Digite a idade da pessoa para adicionar à fila: ")
            fila.adicionar_pessoa(idade)
        elif opcao == "2":
            fila.atender_fila()
        elif opcao == "3":
            idade = input("Digite a idade da pessoa idosa para dar prioridade: ")
            fila.dar_prioridade(idade)
        elif opcao == "4":
            print("Saindo do sistema...")
            break
        else:
            print("Opção inválida. Por favor, tente novamente.")

if __name__ == "__main__":
    main()
