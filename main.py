# main.py

import funcoes

def main():
    pessoas = {}

    while True:
        funcoes.adicionar_pessoa(pessoas)
        continuar = input("Deseja adicionar outra pessoa? (s/n): ")
        if continuar.lower() != 's':
            break

    categorias = funcoes.classificar_idades(pessoas)
    
    print("\nNúmero de pessoas em cada categoria:")
    for categoria, nomes in categorias.items():
        print(f"{categoria}: {len(nomes)}")

    exibir = input("\nDeseja exibir os nomes das pessoas de cada grupo? (s/n): ")
    if exibir.lower() == 's':
        funcoes.exibir_categorias(categorias)

if __name__ == "__main__":
    main()
