import random
import time

def danca_das_cadeiras(participantes):
    cadeiras = len(participantes) - 1

    while cadeiras > 0:
        print(f"\nRodada com {cadeiras} cadeiras e {len(participantes)} participantes.")
        time.sleep(2)  

        indice_eliminado = random.randint(0, len(participantes) - 1)
        jogador_eliminado = participantes.pop(indice_eliminado)
        print(f"{jogador_eliminado} foi eliminado!")

        cadeiras -= 1

    print(f"\nO vencedor é {participantes[0]}!")

participantes = ["Ana", "Bruno", "Carlos", "Daniela", "Eduardo", "Fernanda", "Gabriel", "Helena", "Igor", "Julia"]

danca_das_cadeiras(participantes)
