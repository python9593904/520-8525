#Arquivo com funções para main_banco.py
import sqlite3
from banco.class_banco import Cliente

def conectar_banco():
    conexao = sqlite3.connect('banco_caixa_eletronico.db')
    cursor = conexao.cursor()
    cursor.execute('''
        CREATE TABLE IF NOT EXISTS clientes (
            nome TEXT PRIMARY KEY,
            senha TEXT NOT NULL,
            saldo REAL DEFAULT 0
        )
    ''')
    conexao.commit()
    return onexao

def adicionar_cliente(cliente):
    conexao = conectar_banco()
    cursor = conexao.cursor()
    try:
        cursor.execute('INSERT INTO clientes (nome, senha, saldo)' VALUES (?,?,?)',
                        (cliente.nome, cliente.senha, cliente.saldo))
        conexao.commit()
    except sqlite3.IntegrityError:
        return False
    finally:
        conexao.close()
    return True   