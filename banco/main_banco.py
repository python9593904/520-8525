#Main para aplicação caixa eletronico de banco.
from funcoes_banco import *
from class_banco import Cliente

def menu_principal():
    while True:
        print("\nMenu Principal do Caixa Eletrônico:")
        print("1. Registrar novo cliente")
        print("2. Fazer login")
        print("3. Sair")
        escolha = input("Escolha uma opção: ")

        if escolha == '1':
            registrar_cliente()
        elif escolha == '2':
            fazer_login()
        elif escolha == '3':
            print("Saindo...")
            break
        else:
            print("Opção inválida. Por favor, tente novamente.")

def reg