import unittest
from unittest.mock import patch, mock_open
import funcoes_cadastro
import os

class TesteFuncoesCadastro(unittest.TestCase):
    # Verificando se a função validar_cpf está funcionando
    def teste_validar_cpf(self):
        # CPF Válido teste
        self.assertTrue(funcoes_cadastro.validar_cpf('12345678901'))
        # CPFs inválidos: um curto, um longo e um não numérico.
        self.assertFalse(funcoes_cadastro.validar_cpf('1234567890'))
        self.assertFalse(funcoes_cadastro.validar_cpf('123456789012'))
        self.assertFalse(funcoes_cadastro.validar_cpf('abcde12345'))

    # Verificando a função cadastrar_usuario com entradas válidas.
    @patch('builtins.input', side_effect=['12345678901', 'Test User', '25', 'M', 'Some Address'])
    def teste_cadastrar_usuario(self, mock_inputs):
        usuario = funcoes_cadastro.cadastrar_usuario()
        self.assertEqual(usuario, ['12345678901', 'Test User', 25, 'M', 'Some Address'])

    # Verificando a função cadastrar_usuario com uma idade inválida seguida de uma idade válida.
    @patch('builtins.input', side_effect=['12345678901', 'Test User', 'invalid', '25', 'M', 'Some Address'])
    def teste_cadastrar_usuario_com_idade_invalida(self, mock_inputs):
        usuario = funcoes_cadastro.cadastrar_usuario()
        self.assertEqual(usuario, ['12345678901', 'Test User', 25, 'M', 'Some Address'])

    # Verificando a função cadastrar_usuario com um sexo inválido seguido de um sexo válido.
    @patch('builtins.input', side_effect=['12345678901', 'Test User', '25', 'X', 'M', 'Some Address'])
    def teste_cadastrar_usuario_com_sexo_invalido(self, mock_inputs):
        usuario = funcoes_cadastro.cadastrar_usuario()
        self.assertEqual(usuario, ['12345678901', 'Test User', 25, 'M', 'Some Address'])

    # Verificandoar se a função salvar_em_csv está escrevendo corretamente no arquivo CSV.
    @patch('csv.writer')
    def teste_salvar_em_csv(self, mock_csv_writer):
        mock_file = mock_open()
        with patch('builtins.open', mock_file):
            funcoes_cadastro.salvar_em_csv(['12345678901', 'Test User', 25, 'M', 'Some Address'])
        # Verifica se o arquivo foi aberto para escrita e se o writer do CSV foi chamado.
        mock_file.assert_called_once_with('cadastro.csv', mode='a', newline='', encoding='utf-8')
        mock_csv_writer.assert_called_once()

    # Verificando se a função cpf_ja_cadastrado está funcionando corretamente.
    @patch('builtins.open', new_callable=mock_open, read_data='12345678901;Test User;25;M;Some Address\n')
    def teste_cpf_ja_cadastrado(self, mock_file):
        # Testando um CPF que já está cadastrado e outro que não está.
        self.assertTrue(funcoes_cadastro.cpf_ja_cadastrado('12345678901'))
        self.assertFalse(funcoes_cadastro.cpf_ja_cadastrado('10987654321'))

    # Teste para verificar se a função consultar_cadastro está encontrando e gerando output correta
    @patch('builtins.open', new_callable=mock_open, read_data='12345678901;Test User;25;M;Some Address\n')
    def teste_consultar_cadastro(self, mock_file):
        with patch('builtins.print') as mock_print:
            funcoes_cadastro.consultar_cadastro('12345678901')
            # Verificando se a função print foi chamada com a saída que era esperada
            mock_print.assert_called_with("CPF: 12345678901, Nome: Test User, Idade: 25, Sexo: M, Endereço: Some Address")

    # Verificando se a função excluir_cadastro está funcionando corretamente.
    @patch('builtins.open', new_callable=mock_open, read_data='12345678901;Test User;25;M;Some Address\n10987654321;Another User;30;F;Another Address\n')
    def teste_excluir_cadastro(self, mock_file):
        newline = os.linesep
        funcoes_cadastro.excluir_cadastro('12345678901')
        # Verificando se a função write foi chamada com os dados corretos, excluindo o cadastro requisitado
        mock_file().write.assert_called_with(f'10987654321;Another User;30;F;Another Address{newline}')

    # Verificando se a função buscar_por_nome está encontrando e imprimindo os detalhes corretos dos cadastros
    @patch('builtins.open', new_callable=mock_open, read_data='12345678901;Test User;25;M;Some Address\n10987654321;Another User;30;F;Another Address\n')
    def teste_buscar_por_nome(self, mock_file):
        with patch('builtins.print') as mock_print:
            funcoes_cadastro.buscar_por_nome('Test')
            # Verificando a função print com a saída esperada para um nome existente
            mock_print.assert_called_with("CPF: 12345678901, Nome: Test User, Idade: 25, Sexo: M, Endereço: Some Address")

            # Testa a busca por um nome que não existe nos cadastros.
            funcoes_cadastro.buscar_por_nome('Nonexistent')
            # Verifica se a função print foi chamada com a mensagem de nenhum cadastro encontrado.
            mock_print.assert_called_with("Nenhum cadastro encontrado com esse nome.")

if __name__ == '__main__':
    unittest.main()
