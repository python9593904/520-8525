import os
import sys
from enum import Enum, auto
from collections import Counter

class Fruta(Enum):
    BANANA = auto()
    MELANCIA = auto()
    MORANGO = auto()

    @staticmethod
    def nome(fruta):
        nomes = {Fruta.BANANA: "Banana", Fruta.MELANCIA: "Melancia", Fruta.MORANGO: "Morango"}
        return nomes[fruta]

def limpar_tela():
    os.system('cls' if os.name == 'nt' else 'clear')

def mostrar_menu_principal():
    print("\nQuitanda:")
    print("1: Ver cesta")
    print("2: Adicionar frutas")
    print("3: Checkout")
    print("4: Sair")

def mostrar_menu_frutas():
    print("\nEscolha a fruta desejada:")
    for fruta in Fruta:
        print(f"{fruta.value} - {Fruta.nome(fruta)}")

def adicionar_fruta(cesta, escolha):
    fruta_escolhida = Fruta(escolha)
    quantidade = int(input(f"Quantidade de {Fruta.nome(fruta_escolhida)}: "))
    for _ in range(quantidade):
        cesta.append(Fruta.nome(fruta_escolhida))
    print(f"{quantidade} {Fruta.nome(fruta_escolhida)}{'s' if quantidade > 1 else ''} adicionada(s) com sucesso!")

def calcular_total(cesta):
    precos = {"Banana": 4.50, "Melancia": 6.50, "Morango": 4.00}
    return sum(precos[fruta] for fruta in cesta)

def mostrar_cesta(cesta):
    contagem = Counter(cesta)
    return ', '.join([f"{qtd} {fruta}{'s' if qtd > 1 else ''}" for fruta, qtd in contagem.items()])

def obter_opcao_valida(prompt, opcoes_validas):
    while True:
        try:
            escolha = int(input(prompt))
            if escolha in opcoes_validas:
                return escolha
            else:
                print("Digite uma opção válida!")
        except ValueError:
            print("Por favor, digite um número válido.")

def main():
    cesta = []
    
    while True:
        mostrar_menu_principal()
        opcao = obter_opcao_valida("Digite a opção desejada: ", [1, 2, 3, 4])

        if opcao == 1:
            if cesta:
                print("Cesta de compras:", mostrar_cesta(cesta))
            else:
                print("A cesta está vazia.")
        elif opcao == 2:
            mostrar_menu_frutas()
            escolha_fruta = obter_opcao_valida("Digite a opção desejada: ", [fruta.value for fruta in Fruta])
            adicionar_fruta(cesta, escolha_fruta)
        elif opcao == 3:
            if cesta:
                total = calcular_total(cesta)
                print(f"Total de compras: R$ {total:.2f}")
                print("Cesta de compras:", mostrar_cesta(cesta))
                confirmacao = input("Deseja finalizar a compra? (S/N): ").strip().upper()
                if confirmacao == 'S':
                    print("Checkout realizado com sucesso!")
                    cesta.clear()
                else:
                    print("Checkout cancelado.")
            else:
                print("A cesta está vazia.")
        elif opcao == 4:
            print("Obrigado por visitar a quitanda! Até logo!")
            sys.exit()

        input("\nPressione Enter para continuar...")
        limpar_tela()

if __name__ == "__main__":
    main()
