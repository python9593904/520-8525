def contar_vogais(texto):
    vogais = 'aeiouAEIOUáéíóúÁÉÍÓÚâêôÂÊÔãõÃÕ'
    contagem = 0

    for letra in texto:
        if letra in vogais:
            contagem += 1

    return contagem

def main():
    try:
        with open('faroeste_caboclo.txt', 'r', encoding='utf-8') as arquivo:
            letra_musica = arquivo.read()

        total_vogais = contar_vogais(letra_musica)
        print(f"Total de vogais na música Faroes Caboclo: {total_vogais}")

    except FileNotFoundError:
        print("Arquivo não encontrado. Por favor, verifique o nome do arquivo e tente novmente.")

if __name__ == "__main__":
    main()