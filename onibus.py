class Onibus:
    def __init__(self, placa, modelo):
        self.capacidade_total = 45
        self.capacidade_atual = 0
        self.placa = placa
        self.modelo = modelo
        self.movimento = False

    def embarcar(self, num_pessoas):
        if self.movimento:
            print("Não é possível embarcar com o ônibus em movimento.")
            return

        if self.capacidade_atual + num_pessoas > self.capacidade_total:
            print("Capacidade excedida! Não é possível embarcar tantas pessoas.")
        else:
            self.capacidade_atual += num_pessoas
            print(f"{num_pessoas} pessoas embarcaram.")

    def desembarcar(self, num_pessoas):
        if self.movimento:
            print("Não é possível desembarcar com o ônibus em movimento.")
            return

        if self.capacidade_atual == 0:
            print("O ônibus está vazio. Não há pessoas para desembarcar.")
            return

        if num_pessoas > self.capacidade_atual:
            print("Não há tantas pessoas para desembarcar.")
        else:
            self.capacidade_atual -= num_pessoas
            print(f"{num_pessoas} pessoas desembarcaram.")

    def acelerar(self):
        self.movimento = True
        print("O ônibus começou a se mover.")

    def frear(self):
        self.movimento = False
        print("O ônibus parou.")
