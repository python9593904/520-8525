class FilaBanco:
    def __init__(self):
        self.fila = []

    def adicionar_pessoa(self, idade):
        try:
            idade = int(idade)
            if idade < 0:
                raise ValueError("A idade não pode ser negativa.")
            self.fila.append(idade)
            print(f"Pessoa de {idade} anos adicionada à fila.")
        except ValueError as e:
            print(f"Erro: {e}")

    def atender_fila(self):
        if self.fila:
            pessoa_atendida = self.fila.pop(0)
            print(f"Pessoa de {pessoa_atendida} anos atendida.")
        else:
            print("A fila está vazia, não há ninguém para atender.")

    def dar_prioridade(self, idade):
        try:
            idade = int(idade)
            if idade < 0:
                raise ValueError("A idade não pode ser negativa.")

            if idade >= 65:
                posicao_para_inserir = 0
                for i in range(len(self.fila)):
                    if self.fila[i] >= 65:
                        posicao_para_inserir = i + 1
                self.fila.insert(posicao_para_inserir, idade)
                print(f"Pessoa de {idade} anos recebeu prioridade na fila.")
            else:
                self.adicionar_pessoa(idade)
        except ValueError as e:
            print(f"Erro: {e}")
