lista_produtos = []

catalogo_frutas = {
    '1': 'Banana',
    '2': 'Melancia',
    '3': 'Morango'
}

tabela_precos = {
    'Banana': 4.50,
    'Melancia': 8.00,
    'Morango': 7.00
}

while True:

    print(f'Quitanda\n' \
          f'1: Ver lista de produtos\n' \
          f'2: Adicionar Frutas\n' \
          f'3: Finalizar compra\n' \
          f'4: Sair\n')

    escolha = input('Digite a opção desejada: ')

    if escolha == '1':
        print('Lista de Produtos:')
        for produto in lista_produtos:
            print(produto)
        print('------------------------')

    elif escolha == '2':
        opcoes_menu = '\n'.join([f'{codigo} - {fruta}' for codigo, fruta in catalogo_frutas.items()])
        escolha_fruta = input(f'Escolha a fruta desejada:\n{opcoes_menu}\nDigite a opção desejada: ')
        if escolha_fruta in catalogo_frutas:
            lista_produtos.append(catalogo_frutas[escolha_fruta])
            print(f'{catalogo_frutas[escolha_fruta]} foi adicionado com sucesso!')
            print('------------------------')
        else:
            print('Opção inválida')
            print('------------------------')

    elif escolha == '3':
        if len(lista_produtos) > 0:
            valor_total = 0
            for produto in lista_produtos:
                valor_total += tabela_precos[produto]

            print(f'Total da compra: {valor_total}')
            print(f'Lista de produtos: {lista_produtos}')
        else:
            print('Lista de produtos vazia.')

    elif escolha == '4':
        break

    else:
        print('Opção Inválida')
